package collections;

/*
 * Underlying DS--
 * implements -extends vector
 * duplicates----allowed
 * insertion order--- preserved
 * null insertion ---allowed
 * heterogenous -allows
 * retrieval -----
 * addition/deletion---
 * synchronised-----no
 * initial capacity---NA
 * load factor----NA
 *
 *Stack s=new Stack()
 */
import java.util.*;

public class Stack1 {

	public static void main(String[] args) 
	{
		/*Methods of stack
		 * push(Object obj)--->element
		 * pop()----->element
		 * peek()--->element
		 * int search(Object obj)--->int
		 * empty()---->boolean
		 * */
		Stack s=new Stack();
		s.push("A");
		s.push("B");
		s.push("c");
		System.out.println(s.peek());
		System.out.println(s);
		System.out.println(s.search("A"));
		
	}

}
